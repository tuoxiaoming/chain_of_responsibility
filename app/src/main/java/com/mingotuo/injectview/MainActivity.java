package com.mingotuo.injectview;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.mingotuo.injectview.injection.InjectView;
import com.mingotuo.injectview.injection.InjectViewParse;
import java.io.File;
import java.lang.reflect.Field;
import java.util.List;

public class MainActivity extends AppCompatActivity {

  @InjectView(id = R.id.hello) private TextView mTextView;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    InjectViewParse.Inject(this);
    mTextView.setText("打卡");
    mTextView.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        /**包管理器*/
        PackageManager packageManager = getPackageManager();
        /**获得Intent*/
        Intent intent = packageManager.getLaunchIntentForPackage("com.kdweibo.client");
        Class<? extends ComponentName> componentName = ((Intent) intent).getComponent().getClass();
        Field[] fields = componentName.getDeclaredFields();
        for (Field field : fields) {
          if (!field.getName().equals("mClass")) {
            continue;
          }
          field.setAccessible(true);
          try {
            //field.set(intent.getComponent(), "com.yunzhijia.checkin.homepage.CheckinHomePageActivity");
            field.set(intent.getComponent(),
                "com.yunzhijia.checkin.activity.MobileCheckInActivity");
          } catch (IllegalAccessException e) {
            e.printStackTrace();
            Toast.makeText(MainActivity.this, "打开签到页面失败", Toast.LENGTH_SHORT).show();
          }
        }
        if (intent != null) {
          if (checkApkExist(MainActivity.this, intent)) {
            startActivity(intent);
          } else {
            Toast.makeText(MainActivity.this, "未能打开应用", Toast.LENGTH_SHORT).show();
          }
        } else {
          Toast.makeText(MainActivity.this, "未能打开应用", Toast.LENGTH_SHORT).show();
        }
      }
    });
  }

  private boolean checkApkExist(Context context, Intent intent) {
    List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, 0);
    if (list.size() > 0) {
      return true;
    }
    return false;
  }
}
