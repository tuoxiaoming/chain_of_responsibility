package com.mingotuo.chainofresponsibility.ape;

public abstract class ProgramApe {

  protected int expenses;
  protected String apply;

  public abstract int getExpenses();

  public abstract String getApply();
}
