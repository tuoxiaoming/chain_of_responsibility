package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

/**
 * 老总类
 */
public class Boss extends Leader {

  public Boss() {
    super(20000);
  }

  @Override public void reply(ProgramApe ape) {
    System.out.println(ape.getExpenses());
    System.out.println(ape.getApply());
    System.out.println("Boss: Of course Yes!");
  }
}
