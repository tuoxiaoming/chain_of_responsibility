package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

public abstract class Leader {

  private int canDealExpense;
  private Leader mSuperiorLeader;

  public abstract void reply(ProgramApe ape);

  Leader(int canDealExpense) {
    this.canDealExpense = canDealExpense;
  }

  public void handleRequest(ProgramApe ape) {
    if (ape.getExpenses() <= canDealExpense) {
      reply(ape);
    } else {
      if (mSuperiorLeader != null) {
        mSuperiorLeader.handleRequest(ape);
      } else {
        System.out.println("Goodby my money...... " + ape.getExpenses());
      }
    }
  }

  public void setSuperiorLeader(Leader superiorLeader) {
    this.mSuperiorLeader = superiorLeader;
  }
}
