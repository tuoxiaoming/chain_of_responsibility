package com.mingotuo.chainofresponsibility.leader;

import com.mingotuo.chainofresponsibility.ape.ProgramApe;

/**
 * 小组长类
 */
public class GroupLeader extends Leader {

  public GroupLeader() {
    super(1000);
  }

  @Override public void reply(ProgramApe ape) {
    System.out.println(ape.getExpenses());
    System.out.println(ape.getApply());
    System.out.println("GroupLeader: Of course Yes!");
  }
}
