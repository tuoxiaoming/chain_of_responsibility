package com.mingotuo.bridge;

public class DP2 implements Drawing {
  public void draw_2_Rantanle() {
    System.out.println("使用DP2的程序画矩形");
  }

  public void draw_2_Circle() {
    System.out.println("使用DP2的程序画圆形");
  }

  @Override public void drawRantangle() {
    draw_2_Rantanle();
  }

  @Override public void drawCircle() {
    draw_2_Circle();
  }
}
