package com.mingotuo.bridge;

public class DP1 implements Drawing{

  public void draw_1_Rantanle(){
    System.out.println("使用DP1的程序画矩形");
  }

  public void draw_1_Circle(){
    System.out.println("使用DP1的程序画圆形");
  }

  @Override public void drawRantangle() {
    draw_1_Rantanle();
  }

  @Override public void drawCircle() {
    draw_1_Circle();
  }
}
