package com.mingotuo.bridge;

public abstract class Shape {

  protected Drawing myDrawing;

  public Shape(Drawing drawing) {
    this.myDrawing = drawing;
  }

  abstract public void draw();

  protected void drawRectangle() {
    myDrawing.drawRantangle();
  }

  protected void drawCircle() {
    myDrawing.drawCircle();
  }
}
