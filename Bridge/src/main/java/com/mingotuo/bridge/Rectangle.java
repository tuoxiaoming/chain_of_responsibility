package com.mingotuo.bridge;

public class Rectangle extends Shape {

  Rectangle(Drawing drawing) {
    super(drawing);
  }

  @Override public void draw() {
    drawRectangle();
  }
}
