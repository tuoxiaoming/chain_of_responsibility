package com.mingotuo.bridge;

public class Client {

  public static void main(String[] args){

    DP1 dp1 = new DP1();
    DP2 dp2 = new DP2();
    Rectangle rectangle = new Rectangle(dp1);
    Circle circle = new Circle(dp2);

    rectangle.draw();
    circle.draw();
  }

}
